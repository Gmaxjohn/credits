import React, {useEffect, useRef, useState} from 'react';
import "./user.scss";
import Glavnaya from "../Glavnaya/Glavnaya";
import Layout from "../../components/Layout/Layout";
import {TextField} from "@mui/material";
import pdf from "../../images/pdf.svg";
import xls from "../../images/xls.svg";
import docx from "../../images/docx.svg";
import iks from "../../images/iks.svg";
import basket from "../../images/delete.svg";
import Pencil from "../../images/edit.svg";
import checked from "../../images/checked.svg";
import plus from "../../images/plus.svg";
import graph from "../../images/sozdat-grafik.svg";
import {toast} from "react-toastify";


function User(props) {
    const fileData = [{
        img: pdf,
    }, {
        img: xls,
    }, {
        img: docx,
    }];

    const forCycle = [];
    const forCycle2 = [];

    const forCycleFunc = () => {
        for (let i = 1; i < 37; i++) {
            forCycle.push(i)
            forCycle2.push(i + 36)
        }
    }
    forCycleFunc();

    const inpt1 = useRef()
    const inpt2 = useRef()
    const inpt3 = useRef()

    const [inputVal1, setInputVal1] = useState(null)
    const [inputVal2, setInputVal2] = useState(null)
    const [inputVal3, setInputVal3] = useState(null)

    const getInputVal = (file, event) => {
        switch (file) {
            case "pdf": {
                const newFileName = inpt1.current.value.replace(/^.*\\/, "")
                if (newFileName.substring(newFileName.length - 3) === "pdf") {
                    setInputVal1(newFileName)
                    toast.success("Success")
                } else {
                    toast.error("Only PDF files")
                }
            }
                break;
            case "xls": {
                const newFileName = inpt2.current.value.replace(/^.*\\/, "")
                if (newFileName.substring(newFileName.length - 3) === "xls" || newFileName.substring(newFileName.length - 4) === "xlsx") {
                    setInputVal2(newFileName)
                    toast.success("Success")
                } else {
                    toast.error("Only XLS or XLSX files")
                }
            }
                break;
            case "docx": {
                const newFileName = inpt3.current.value.replace(/^.*\\/, "")
                if (newFileName.substring(newFileName.length - 4) === "docx" || newFileName.substring(newFileName.length - 3) === "doc") {
                    setInputVal3(newFileName)
                    toast.success("Success")
                } else {
                    toast.error("Only DOC or DOCX files")
                }
            }
                break;
            default: {

            }
        }
    }

    const RemoveFile = (file) => {
        switch (file) {
            case "pdf": {
                if (inputVal1 === "" || inputVal1 === null) {
                    toast.error("You don't have any files")
                } else {
                    setInputVal1("")
                    toast.info("File removed successfully")
                }
            }
                break;
            case "xls": {
                if (inputVal2 === "" || inputVal2 === null) {
                    toast.error("You don't have any files")
                } else {
                    setInputVal2("")
                    toast.info("File removed successfully")
                }

            }
                break;
            case "docx": {
                if (inputVal3 === "" || inputVal3 === null) {
                    toast.error("You don't have any files")
                } else {
                    setInputVal3("")
                    toast.info("File removed successfully")
                }
            }
                break;
            default: {

            }
        }
    }

    const [show, setShow] = useState(false)

    const showGraph = () => {
        setShow(true)
    }

    const [selBg, setSelBg] = useState(2)

    const [forBg, setForBg] = useState("#CCD6E9")

    const changedSelect = (event, index) => {
        setSelBg(index)
        document.querySelector(".selectBg" + index).classList.remove(".data1")
        switch (event.target.value) {
            case "oplachen": {
                setForBg("#00BF36")
            }
                break;
            case "prosrochen": {
                setForBg("#FF4B4B")
            }
                break;
            case "skoro": {
                setForBg("#F5C257")
            }
                break;
            case "zaplanirovan": {
                setForBg("#CCD6E9")
            }
                break;
            default: {
                setForBg("#CCD6E9")
            }
        }

    }

    const changedSelect2 = (event, index) => {
        setSelBg(index)
        document.querySelector(".selectBg" + index).classList.remove(".data1")
        switch (event.target.value) {
            case "oplachen": {
                setForBg("#00BF36")
            }
                break;
            case "prosrochen": {
                setForBg("#FF4B4B")
            }
                break;
            case "skoro": {
                setForBg("#F5C257")
            }
                break;
            case "zaplanirovan": {
                setForBg("#CCD6E9")
            }
                break;
            default: {
                setForBg("#CCD6E9")
            }
        }

    }

    useEffect(() => {
        document.querySelector(".selectBg" + selBg).style.backgroundColor = forBg
    }, [selBg])

    const [edit, setEdit] = useState(false)

    const [editNum, setEditNum] = useState(1)

    const editInfo = (item) => {
        setEdit(true)
        setEditNum(item)
    }


    return (<Layout>
        <div className="user">
            <div className="for-padding">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-xl-9 p-0">
                            <div className="row">
                                <div className="col-xl-3">
                                    <div className="input-name">
                                        ИНН
                                    </div>
                                    <input type="text" className="form-control" placeholder="563565286576"/>
                                </div>
                                <div className="col-xl-3">
                                    <div className="input-name">
                                        КПП
                                    </div>
                                    <input type="text" className="form-control" placeholder="563565286576"/>
                                </div>
                                <div className="col-xl-3">

                                    <div className="input-name">
                                        БИК
                                    </div>
                                    <input type="text" className="form-control" placeholder="563565286576"/>
                                </div>
                                <div className="col-xl-3">
                                    <div className="input-name">
                                        Р/С
                                    </div>
                                    <input type="text" className="form-control" placeholder="563565286576"/>
                                </div>
                            </div>

                            <div className="row for-mt">
                                <div className="col-xl-9">
                                    <div className="row">
                                        <div className="col-xl-6">
                                            <div className="input-name">
                                                ФИО
                                            </div>
                                            <input type="text" className="form-control"
                                                   placeholder="Джанибеков Джабраил Оглы Маджонгович"/>
                                        </div>
                                        <div className="col-xl-6">
                                            <div className="input-name">
                                                Назначение платежа
                                            </div>
                                            <input type="text" className="form-control"
                                                   placeholder="Оплата кредита на покопку транспортного средства"/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-3">
                                    <div className="col-xl">
                                        <div className="input-name">
                                            К/С
                                        </div>
                                        <input type="text" className="form-control" placeholder="563565286576"/>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div className="col-xl-3">
                            <div className="input-name">
                                Прикрепленные документы
                            </div>
                            <div className="row for-mt2 flex-nowrap">
                                <div className="col-xl-11">
                                    <div className="row">
                                        <div className="col-xl-4">
                                            <div className="files">
                                                <div className="iks">
                                                    <img onClick={() => RemoveFile("pdf")} src={iks} alt=""/>
                                                </div>
                                                <div className="img-file">
                                                    <label
                                                        style={{cursor: "pointer"}}
                                                        htmlFor={`input1`}>
                                                        <img src={pdf} alt=""/>
                                                    </label>
                                                    <input
                                                        ref={inpt1}
                                                        type="file" id={`input1`}
                                                        style={{display: "none", visibility: "none"}}
                                                        onChange={() => getInputVal("pdf")}
                                                    />

                                                    <div className="file-name">
                                                        {inputVal1}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="files">
                                                <div className="iks">
                                                    <img onClick={() => RemoveFile("xls")} src={iks} alt=""/>
                                                </div>
                                                <div className="img-file">
                                                    <label
                                                        style={{cursor: "pointer"}}
                                                        htmlFor={`input2`}>
                                                        <img src={xls} alt=""/>
                                                    </label>
                                                    <input
                                                        ref={inpt2}
                                                        type="file" id={`input2`}
                                                        style={{display: "none", visibility: "none"}}
                                                        onChange={() => getInputVal("xls")}
                                                    />

                                                    <div className="file-name">
                                                        {inputVal2}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-4">
                                            <div className="files">
                                                <div className="iks">
                                                    <img onClick={() => RemoveFile("docx")} src={iks} alt=""/>
                                                </div>
                                                <div className="img-file">
                                                    <label
                                                        style={{cursor: "pointer"}}
                                                        htmlFor={`input3`}>
                                                        <img src={docx} alt=""/>
                                                    </label>
                                                    <input
                                                        ref={inpt3}
                                                        type="file" id={`input3`}
                                                        style={{display: "none", visibility: "none"}}
                                                        onChange={() => getInputVal("docx")}
                                                    />

                                                    <div className="file-name">
                                                        {inputVal3}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-xl-1">
                                    <div className="for-height">
                                        <div className="plus">
                                            <img src={plus} alt=""/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={show ? "d-none" : "for-padding"}>
                <div className="box">
                    <div className="box-content">
                        <div className="inputs">
                            <div className="input-title">
                                Сумма платежа
                            </div>
                            <input type="text" placeholder="1 000 000" className="form-control sum-input"/>
                        </div>
                        <div className="inputs">
                            <div className="input-title">
                                Количство месяцев
                            </div>
                            <input type="text" placeholder="72" className="form-control sum-input"/>
                        </div>
                        <div className="inputs">
                            <div className="input-title">
                                Дата платежа
                            </div>
                            <input type="date" className="form-control sum-input"/>
                        </div>
                    </div>
                    <div onClick={showGraph} className="graph">
                        <img src={graph} alt=""/>
                        <div className="create-graph">
                            Создать график
                        </div>
                    </div>

                </div>
            </div>
            <div className={show ? "for-height2" : "d-none"}>
                <div className="for-padding for-height1">
                    <div className="row">
                        <div className="col-xl-6">
                            <table>
                                <thead>
                                <tr>
                                    <th>
                                        №
                                    </th>
                                    <th>
                                        Дата
                                    </th>
                                    <th>
                                        Сумма
                                    </th>
                                    <th>
                                        Статус
                                    </th>
                                    <th>
                                        Остаток
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {forCycle.map((item, index) => (<tr key={index}>
                                    <td className="for-margin nomerovka">
                                        {item}
                                    </td>
                                    <td className="for-margin data">
                                        <select className="chislo" name="data">
                                            <option value="2004">12.12.2004</option>
                                            <option value="2003">07.09.2003</option>
                                            <option value="2002">22.04.2002</option>
                                            <option value="2001">11.06.2001</option>
                                        </select>
                                    </td>
                                    <td className="for-margin">
                                        <input className="summa" type="text" placeholder="1 000 000"/>
                                    </td>
                                    <td className="for-margin status">
                                        <select onChange={(event) => changedSelect(event, index)} name="data"
                                                className={`data data1 selectBg${index}`}>
                                            <option style={{background: "#CCD6E9"}} value="zaplanirovan"
                                                    className="zaplanirovan">Запланирован
                                            </option>
                                            <option style={{background: "#00BF36"}} value="oplachen"
                                                    className="oplachen">Оплачен
                                            </option>
                                            <option style={{background: "#FF4B4B"}} value="prosrochen"
                                                    className="prosrochen">Просрочен
                                            </option>
                                            <option style={{background: "#F5C257"}} value="skoro"
                                                    className="skoro">Скоро оплата
                                            </option>
                                        </select>
                                    </td>
                                    <td className="for-margin">
                                        <input className="summa" type="text" placeholder="45 000 000"/>
                                    </td>
                                    <td className="for-margin d-flex">
                                        <div className={`edit`}>
                                            <img src={Pencil} alt=""/>
                                        </div>
                                        <div className="delete">
                                            <img src={basket} alt=""/>
                                        </div>
                                    </td>
                                </tr>))}
                                </tbody>
                            </table>
                        </div>
                        <div className="col-xl-6">
                            <table>
                                <thead>
                                <tr>
                                    <th>
                                        №
                                    </th>
                                    <th>
                                        Дата
                                    </th>
                                    <th>
                                        Сумма
                                    </th>
                                    <th>
                                        Статус
                                    </th>
                                    <th>
                                        Остаток
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                {forCycle2.map((item, index) => (<tr key={index}>
                                    <td className="for-margin nomerovka">
                                        {item}
                                    </td>
                                    <td className="for-margin data">
                                        <select className="chislo" name="data">
                                            <option value="2004">12.12.2004</option>
                                            <option value="2003">07.09.2003</option>
                                            <option value="2002">22.04.2002</option>
                                            <option value="2001">11.06.2001</option>
                                        </select>
                                    </td>
                                    <td className="for-margin">
                                        <input className="summa" type="text" placeholder="1 000 000"/>
                                    </td>
                                    <td className="for-margin status">
                                        <select onChange={(event) => changedSelect2(event, index + 36)} name="data"
                                                className={`data data1 selectBg${index + 36}`}>
                                            <option style={{background: "#CCD6E9"}} value="zaplanirovan"
                                                    className="zaplanirovan">Запланирован
                                            </option>
                                            <option style={{background: "#00BF36"}} value="oplachen"
                                                    className="oplachen">Оплачен
                                            </option>
                                            <option style={{background: "#FF4B4B"}} value="prosrochen"
                                                    className="prosrochen">Просрочен
                                            </option>
                                            <option style={{background: "#F5C257"}} value="skoro"
                                                    className="skoro">Скоро оплата
                                            </option>
                                        </select>
                                    </td>
                                    <td className="for-margin">
                                        <input className="summa" type="text" placeholder="45 000 000"/>
                                    </td>
                                    <td className="for-margin d-flex">
                                        <div className={`edit`}>
                                            <img src={Pencil} alt=""/>
                                        </div>
                                        <div className="delete">
                                            <img src={basket} alt=""/>
                                        </div>
                                    </td>
                                </tr>))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </Layout>);
}

export default User;