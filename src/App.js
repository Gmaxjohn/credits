import {Redirect, Route, Switch} from "react-router-dom";
import {TextField} from "@mui/material";
import User from "./pages/User/User";
import Car from "./pages/Car/Car";
import Key from "./pages/Key/Key";
import Glavnaya from "./pages/Glavnaya/Glavnaya";
import Payments from "./pages/Payments/Payments";
import Comments from "./pages/Comments/Comments";
import {ToastContainer} from "react-toastify";


function App() {
    return (<div className="App">
        {/*<TextField id="outlined-basic" label="ИНН" variant="outlined" placeholder="number" />*/}
        <Switch>
            <Route exact path="/user-info" component={User}/>
            <Route exact path="/about-car" component={Car}/>
            <Route exact path="/key-info" component={Key}/>
            <Route exact path="/">
                <Redirect to="/user-info"/>
            </Route>
            <Route exact path="/glavnaya" component={Glavnaya}/>
            <Route exact path="/payments" component={Payments}/>
            <Route exact path="/comments" component={Comments}/>

        </Switch>
        <ToastContainer/>

    </div>);
}

export default App;
